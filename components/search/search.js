// companies/search/search.js
import {getGoods_count} from "../../http/api"
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    
  },

  /**
   * 组件的初始数据
   */
  data: {
    goodsCount:null
  },
  lifetimes:{
    async attached(){
      const arr = await getGoods_count()
      this.setData({
        goodsCount:arr.goodsCount
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {

  }

})
