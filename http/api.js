import { http } from "./index"
//首页数据
export function getIndex() {
  return http("index/index").then(res => {
    return res.data.data
  })
}
// 专题
export function getTopic(data) {
  return http("topic/list", data).then(res => {
    return res.data.data
  })
}
// 商品搜索栏总数
export function getGoods_count() {
  return http("goods/count").then(res => {
    return res.data.data
  })
}
//分类
export function getCatalog() {
  return http("catalog/index").then(res => {
    return res.data.data
  })
}
//分类具体
export function getCatalogs(data) {
  return http("catalog/current", data).then(res => {
    return res.data.data
  })
}
//购物车
export function getCart() {
  return http("cart/index").then(res => {
    return res.data.data
  })
}
//购物车编辑商品数量
export function getCartUpdata(data) {
  return http("cart/update", data, 'post').then(res => {
    return res.data.data
  })
}
//购物车商品删除
export function getCartDelete(data) {
  return http("cart/delete", data, 'post').then(res => {
    return res.data.data
  })
}
//购物车选中
export function getCartChecked(data) {
  return http("cart/checked", data, 'post').then(res => {
    return res.data.data
  })
}
//登录
export function getAuthWX(data) {
  return http("auth/loginByWeixin", data, 'post').then(res => {
    return res.data.data
  }).catch((res) => {
    wx.showToast({
      title: "微信登陆失败",
      icon: "none"
    })
  })
}
//商品详情
export function getDoods(data) {
  return http("goods/detail", data, 'get').then(res => {
    return res.data.data
  })
}
//商品大家都在看
export function getDoodsRel(data) {
  return http("goods/related", data, 'get').then(res => {
    return res.data.data
  })
}
//购物车数量

export function getGoodscount() {
  return http("cart/goodscount", {}, 'get').then(res => {
    return res.data.data
  })
}

//添加购物车
export function getGoodAdd(data) {
  return http("cart/add", data, 'post').then(res => {
    if (res.data.errno == 0) {
      wx.showToast({
        title: '添加成功'
      });

      return res.data.data
    } else {
      wx.showToast({
        image: '/static/images/icon_error.png',
        title: _res.errmsg,
        mask: true
      });
    }
  })
}
//具体分类内容
export function getCategory(data) {
  return http("goods/category", data).then(res => {
    return res.data.data
  })
}
export function getGoodsList(data) {
  return http("goods/list", data).then(res => {
    return res.data.data
  })
}
export function getBrand(data) {
  return http("brand/detail", data).then(res => {
    return res.data.data
  })
}
//专题接口
export function getTopicCom(data) {
  return http("topic/detail", data).then(res => {
    return res.data.data
  })
}
//专题评论
export function getTopicComList(data) {
  return http("comment/list", data).then(res => {
    return res.data.data
  })
}
//专题推荐
export function getTopicRel(data) {
  return http("topic/related", data).then(res => {
    return res.data.data
  })
}