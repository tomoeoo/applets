// const ApiRootUrl = 'https://pc.raz-kid.cn/api/';
const ApiRootUrl = 'http://39.106.194.179:8360/api/';
var appInst =  getApp();

export const http = function (url, data, method = "GET") {
    return new Promise((resolve, reject) => {
        wx.request({
            url: ApiRootUrl + url,
            data: data,
            header: {
                'content-type': 'application/json',
                'X-Nideshop-Token': wx.getStorageSync('token')?wx.getStorageSync('token'):''
            },
            method: method.toUpperCase(),
            dataType: 'json',
            responseType: 'text',
            success: (res) => {
                if (res.statusCode >= 200 && res.statusCode < 300 || res.statusCode === 304) {
                    if (res.data.errno === 0) {
                        resolve(res)
                    } else {
                        wx.showToast({
                            title: res.data.errmsg,
                            icon: "none"
                        })
                        reject(res)
                    }
                }
            },
            fail: (res) => {
                wx.showToast({
                    title: res,
                    icon: "none"
                })
            },
            complete: () => { }
        });
    })
}