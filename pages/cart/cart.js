// pages/cart/cart.js
import { getCart, getCartUpdata, getCartDelete, getCartChecked } from '../../http/api';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cartList: [],
    editBoo: true,
    cartTotal: [],
    strs: null,
    str: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async getCartDelete() {
    const arr = await getCartDelete({ productIds: this.data.str })

    this.setData({
      cartList: arr.cartList.map(item => {
        item.checked = 0
        return item
      }),
      cartTotal: arr.cartTotal
    })
    const strs = []
    this.data.cartList.forEach(item => {
      strs.push(item.product_id)
    })
    this.setData({
      strs: strs.join(),
      str:''
    })
  },
  editBoos() {
    this.setData({
      editBoo: !this.data.editBoo
    })
    if (!this.data.editBoo) {
      this.setData({
        cartList: this.data.cartList.filter(item => {
          item.checked = 0
          return true
        }),
        str: ''
      })
    } else {
      this.onLoad()
    }
  },
  async getCartUpdata(event) {
    const { goods_id: goodsId, id, number, product_id: productId } = event.currentTarget.dataset.datas
    const num = event.currentTarget.dataset.num
    console.log(0)
    console.log(num)
    if (number + (num - 0) > 0) {
      const arr = await getCartUpdata({
        goodsId, id, number: number + (num - 0), productId
      })
      this.setData({
        cartList: arr.cartList,
        cartTotal: arr.cartTotal
      })
    }
  },
  async getCartCheckeds() {
    if (this.data.editBoo) {
      let arr = null
      if (this.data.strs == this.data.str) {
        arr = await getCartChecked({
          isChecked: 0, productIds: this.data.strs
        })
      } else {
        arr = await getCartChecked({
          isChecked: 1, productIds: this.data.strs
        })
      }
      this.setData({
        cartList: arr.cartList,
        cartTotal: arr.cartTotal
      })
      const str = []
      this.data.cartList.forEach(item => {
        if (item.checked) {
          str.push(item.product_id)
        }
      })
      this.setData({
        str: str.join()
      })
    } else {
      if (this.data.strs == this.data.str) {
        this.setData({
          cartList: this.data.cartList.map(item => {
            item.checked = 0
            return item
          })
        })
      } else {
        this.setData({
          cartList: this.data.cartList.map(item => {
            item.checked = 1
            return item
          })
        })
      }
      const str = []
      this.data.cartList.forEach(item => {
        if (item.checked) {
          str.push(item.product_id)
        }
      })
      this.setData({
        str: str.join()
      })
      console.log(str.join())
    }
  },
  async getCartChecked(event) {
    if (this.data.editBoo) {
      const { checked: isChecked, product_id: productIds } = event.currentTarget.dataset.datas
      const arr = await getCartChecked({
        isChecked: isChecked ? 0 : 1, productIds
      })
      this.setData({
        cartList: arr.cartList,
        cartTotal: arr.cartTotal
      })
      const str = []
      this.data.cartList.forEach(item => {
        if (item.checked) {
          str.push(item.product_id)
        }
      })
      this.setData({
        str: str.join()
      })
    } else {
      console.log(0)
      const { product_id } = event.currentTarget.dataset.datas
      this.setData({
        cartList: this.data.cartList.map(item => {
          if (item.product_id == product_id) {
            item.checked = item.checked ? 0 : 1
          }
          return item
        })
      })
      const str = []
      this.data.cartList.forEach(item => {
        if (item.checked) {
          str.push(item.product_id)
        }
      })
      this.setData({
        str: str.join()
      })
      console.log(str.join())
    }
  },
  async onLoad(options) {
    const arr = await getCart()
    console.log(arr)
    this.setData({
      cartList: arr.cartList,
      cartTotal: arr.cartTotal
    })
    const strs = []
    this.data.cartList.forEach(item => {
      strs.push(item.product_id)
    })
    this.setData({
      strs: strs.join()
    })
    const str = []
    this.data.cartList.forEach(item => {
      if (item.checked) {
        str.push(item.product_id)
      }
    })
    this.setData({
      str: str.join()
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})