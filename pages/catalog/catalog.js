// pages/catalog/catalog.js
import { getCatalog, getCatalogs } from '../../http/api';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoryList: [],
    currentCategory: null,
    id:null
  },
  async tablis(event){
    const datas=event.currentTarget.dataset
    this.setData({
      id:datas.id
    })
    const arr=await getCatalogs({id:datas.id})
    this.setData({
      currentCategory:arr.currentCategory
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    const arr = await getCatalog()
    this.setData({
      categoryList: arr.categoryList,
      currentCategory: arr.currentCategory,
      id:arr.categoryList[0].id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})