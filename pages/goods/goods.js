// pages/goods/goods.js
import { getDoods,getDoodsRel,getGoodscount,getGoodAdd } from "../../http/api"
var WxParse = require('../../wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    gallery: [],
    info: {},
    comment: {},
    attribute: [],
    issue:[],
    goodsList:[],
    cart_num:null,
    number:1,
    goodsId:null,
    productId:null,
    cartBoo:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  cutNum(){
    if(this.data.number-1>0){
      this.setData({
        number:this.data.number-1
      })
    }
  },
  addNum(){
    this.setData({
      number:this.data.number+1
    })
  },
  cartBoos(){
    this.setData({
      cartBoo:true
    })
  },
  async cartBoosAdd(){
    if(this.data.cartBoo){
      this.setData({
        cartBoo:false
      })
    }else{
      const arr=await getGoodAdd()

      this.setData({
        
      })
    }
  },
  async onLoad(options) {
    console.log(options)
    const arr = await getDoods(options)
    const arr2 = await getDoodsRel(options)
    const obj= await getGoodscount()
    this.setData({
      gallery: arr.gallery,
      info: arr.info,
      comment: arr.comment,
      attribute: arr.attribute,
      issue:arr.issue,
      goodsList:arr2.goodsList,
      cart_num:obj.cartTotal.goodsCount,
      productId:arr.productList[0].id,
      goodsId:arr.productList[0].goods_id
    })
    var that = this;
    WxParse.wxParse('article', 'html', arr.info.goods_desc, that);

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})