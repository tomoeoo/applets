//Page Object
import { http } from "../../http/index.js"
import { getIndex } from "../../http/api"
Page({
    data: {
        banner: [],
        brandList:[],
        newGoodsList:[],
        hotGoodsList:[],
        topicList:[],
        categoryList:[],
        channel:[]
    },
    async onLoad(options) {
        const arr = await getIndex()
        this.setData({
            banner:arr.banner,
            brandList:arr.brandList,
            newGoodsList:arr.newGoodsList,
            hotGoodsList:arr.hotGoodsList,
            topicList:arr.topicList,
            categoryList:arr.categoryList,
            channel:arr.channel
        })
    },
    onReady: function () {

    },
    onShow: function () {

    },
    onHide: function () {

    },
    onUnload: function () {

    },
    onPullDownRefresh: function () {

    },
    onReachBottom: function () {

    },
    onShareAppMessage: function () {

    },
    onPageScroll: function () {

    },
    //item(index,pagePath,text)
    onTabItemTap: function (item) {

    }
});