// pages/topic/topic.js
import { getTopic } from "../../http/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    datas: {
      page: 1,
      size: 10
    },
    list: [],
    totalPages: null,
    scrollTop:0
  },
  async nextPage(event) {
    if (this.data.datas.page < this.data.totalPages) {
      this.setData({
        datas: {
          page: this.data.datas.page + 1,
          size: 10
        },
      })
      const arr = await getTopic(this.data.datas)
      this.setData({
        list: arr.data,
      })
      this.setData({
        scrollTop:0
      })
    }
  },
  async prevPage() {
    if (this.data.datas.page > 1) {
      this.setData({
        datas: {
          page: this.data.datas.page - 1,
          size: 10
        },
      })
      const arr = await getTopic(this.data.datas)
      this.setData({
        list: arr.data,
      })
      this.setData({
        scrollTop:0
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    const arr = await getTopic(this.data.datas)
    console.log(arr)
    this.setData({
      list: arr.data,
      totalPages: arr.totalPages
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})