// pages/topicComment/topicComment.js
import {getTopicCom,getTopicComList,getTopicRel} from '../../http/api';
var WxParse = require('../..//wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    recList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    const arr = await getTopicCom(options)
    const arr2 = await getTopicComList({valueId: options.id,
      typeId: 1,
      size: 5})
    const arr3 = await getTopicRel(options)
    console.log(arr2.data)
    var that = this;
    WxParse.wxParse('article', 'html', arr.content, that);
    this.setData({
      list:arr2.data,
      recList:arr3
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})