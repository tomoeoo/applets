// pages/ucenter/index/index.js
var appInst = getApp();
import { getAuthWX } from '../../../http/api';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dialogBoo: false,
    idBoo: true,
    token: '',
    userInfo: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  dialogBind() {
    if (!this.data.token) {
      this.setData({
        dialogBoo: !this.data.dialogBoo
      })
    }
  },
  zuzhi() { },
  onWechatLogin(event) {
    console.log(event)
    if (event.detail.errMsg == "getUserInfo:ok") {
      wx.login({
        complete: async (res) => {
          console.log(res.code)
          const arr = await getAuthWX({
            code: res.code,
            userInfo: event.detail
          })
          console.log(arr)
          if (arr) {
            wx.setStorageSync("token", arr.token);
            wx.setStorageSync("userInfo", arr.userInfo);
          }
          this.setData({
            token: arr.token,
            userInfo: arr.userInfo,
            dialogBoo: !this.data.dialogBoo
          })
        }
      })
    }
  },
  onLoad: function (options) {
    console.log(appInst)
    this.setData({
      token: appInst.globalData.token,
      userInfo: appInst.globalData.userInfo
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})